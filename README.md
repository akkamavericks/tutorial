# Ansible Tutorial

## Introduction
Several Ansible tutorials, experiments, and useful constructs are given in this repo. Use them at your convenience.

No guarantees, usage is at your own risk.

Enjoy,

Jean-Marc van Leerdam.

[(CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)

## Ansible vault examples
Some examples use encrypted values or files. To use them, Ansible needs the password that was used to encrypt them. You need to create a file in the root
folder of this repo called `.vault_pass.txt`. Fill it with the password, and Ansible will use it. The password used in this repo is: `MyLocalSecretP4ssword`

(the `.vault_pass.txt` file is `.gitignored`, and therefor not part of the cloned repository)

* View/modify the `secret.yml` file:

    `ansible-vault edit group_vars/all/secret.yml`

* Use the values in a playbook:

    `ansible-playbook show_secrets.yml`
    
## Example 1: Place lists into configuration files
Often, the variables that need to be injected into files are not single values, but consist of lists of strings, or even lists of structured data.
Ansible has a lot of filters that help in formatting the structured data, reducing the need to do string manipulations yourself

`myconfig.conf` is an example configuration file that needs a list of users. The list is given in `group_vars/all/vars.yml` and is stored in
the var `userlist`:

```yaml
userlist:
  - Peter
  - John
  - Ann
  - Beth
```

The input template `input/example1/myconfig.conf` contains:
```
application {
  users = {{ userlist | to_json }}
}
```

When you run the command `ansible-playbook example1.yml`, the result in `target/example1/myconfig.conf` contains:

```
application {
  users = ["Peter", "John", "Ann", "Beth"]
}
```
